import XCTest
@testable import myFirstPackage

final class myFirstPackageTests: XCTestCase {
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(myFirstPackage().text, "Hello, World!")
    }
}
