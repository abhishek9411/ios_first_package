//
//  File.swift
//  
//
//  Created by Abhishek Singh on 30/06/23.
//

import Foundation
import SwiftUI

@available(iOS 13.0, *)
struct FirstView : View {
    
    @available(macOS 10.15, *)
    var body: some View {
        Text("this is my first package")
    }
}
